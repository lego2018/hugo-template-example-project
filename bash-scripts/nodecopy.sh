#!/usr/bin/env bash

# Make dependencies directory inside assets and copy dependencies if directory does not exist
if [ ! -d "assets/dependencies" ]
then
    mkdir assets/dependencies

    # Copy jQuery Slim, Bootstrap JS, Bootstrap SCSS
    ditto -V node_modules/jquery/dist/jquery.js assets/dependencies/
    ditto -V node_modules/bootstrap/dist/js/bootstrap.bundle.js assets/dependencies/
    ditto -V node_modules/bootstrap/scss assets/dependencies/bootstrap-scss/

    # Lazy Loading Images: http://dinbror.dk/blog/blazy/?ref=github
    # See also 'node_modules/blazy/example/index.html'
    ditto -V node_modules/blazy/blazy.js assets/dependencies/

    # Font Awesome Pro
    # We use Font Awesome SVG via JS (recommended way). If you want to use less than "all" just
    # change the filename below (see available files in node_modules/@fortawesome/fontawesome-pro/js/)
    ditto -V node_modules/@fortawesome/fontawesome-pro/js/all.js static/fontawesome-all.js

    # Rename any CSS files to SCSS
    mv assets/dependencies/flickity/dist/flickity.css assets/dependencies/flickity/dist/flickity.scss

    echo ------------------------------
    echo Dependancies copied!
    echo ------------------------------
    echo
fi
