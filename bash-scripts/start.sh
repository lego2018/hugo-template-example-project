#!/usr/bin/env bash

clear

echo
echo "--------------------------------------------------------------------------------"
echo "This script installs the 'node_modules'."
echo "--------------------------------------------------------------------------------"
echo "Installing 'node_modules'..."
echo

npm install

echo
echo "Cleaning up 'node_modules'..."
echo

npm prune

echo
echo "Copy Node files to 'assets/dependencies'..."
echo

# The '-s' switch stands for silent
nps -s nodecopy

echo
echo "And some more production files..."
echo

# The '-s' switch stands for silent
nps -s copy

echo "...ready."

echo "... last but not least I discard .DS_Store files"
echo

del static/.DS_Store

del build/local/.DS_Store
del build/stage/.DS_Store
del build/production/.DS_Store

echo "...ready."

echo "--------------------------------------------------------------------------------"
echo "This script just installed 'node_modules' and some production files."
echo "--------------------------------------------------------------------------------"
echo
echo "--------------------------------------------------------------------------------"
echo "You can start the development environment with 'npm run dev'! Happy Development!"
echo "--------------------------------------------------------------------------------"
echo
exit 0
