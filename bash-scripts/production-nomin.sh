#!/usr/bin/env bash

# The 'nps -s' switch stands for silent

while true; do
  read -p "Did you customize htaccess, robots, config, Rake and deploy? (y/n)" yn
  case $yn in
    [Yy]* ) nps -s clean.production;
            nps -s build.productionNomin.hugo;
            break;;

    [Nn]* ) clear;
            echo "---------------------------------------------------"
            echo "Okay, Please do that before going on..."
            echo "Customize .htaccess, humans, config, Rake and deploy"
            echo "and restart production-nomin build."
            echo "---------------------------------------------------"
            exit;;

      * ) echo "Please answer with Yes or No.";;
  esac
done
