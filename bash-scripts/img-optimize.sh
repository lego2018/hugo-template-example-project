#!/usr/bin/env bash

size=800x
quality=85

if [[ -z $1 ]]; then
    echo Please specify an absolute path
    exit 1
fi

pushd $1
magick mogrify -format jpg *.png
magick mogrify -resize $size *.jpg
magick mogrify -strip -quality $quality *.jpg
popd

# https://zwbetz.com/optimize-images-and-reduce-page-load-times-with-imagemagick/
#
# Usage:
# bash-scripts/img-optimize.sh /Path/to/Pics
