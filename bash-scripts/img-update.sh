#!/usr/bin/env bash

# Update images and elements in 'static'

rsync --delete -raz assets/elements static
rsync --delete -raz assets/images static

echo ------------------------------
echo Images and Elements updated!
echo ------------------------------
echo
