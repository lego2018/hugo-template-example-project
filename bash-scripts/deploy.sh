#!/usr/bin/env bash

# rsync can also be started with 'Rakefile.rb'

# Variables
USER=ssh-w013c619
HOST=w013c619.kasserver.com
DIR=/www/htdocs/w013c619/test.leomerkel.de/
BUILD=build/production

echo
echo "-------------------------------------------------------------------------------"
echo "This script transfers the production files from $BUILD to the server."
echo "-------------------------------------------------------------------------------"
echo "Transfer in progress..."
echo

# Use Variables from above
# rsync Parameter mean:
#     a: transfer rights
#     v: verbose
#     e: Chooses remote shell (e. g. -e ssh)

rsync -avhe ssh --delete --exclude '.DS_Store' $BUILD $USER@$HOST:$DIR

echo
echo "...ready."
echo "-------------------------------------------------------------------------------"
echo
exit 0
