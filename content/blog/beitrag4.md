+++
title        = "My fourth post"
description  = "Description of my fourth post"
date         = 2018-07-28

slug         = "post-4"
bodyclass    = ""

categories   = []
tags         = []
+++

Text from Content: This is the summary of the fourth post and covers several issues. After this paragraph there's a More-Tag.

<!--more-->

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus temporibus ea, hic vel debitis alias dolores quasi unde ratione? Rerum omnis explicabo totam rem possimus, sed cum eos sint numquam accusantium illo voluptate esse ea architecto dicta molestiae, asperiores assumenda amet sunt quos maiores nostrum tempore quas ab? Ipsum, molestiae?
