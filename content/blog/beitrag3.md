+++
title        = "My third post"
description  = "Description of my third post"
date         = 2018-07-27

slug         = "post-3"
bodyclass = ""

categories   = ["Category 1"]
tags         = ["One", "Three", "Eight"]
+++

Text from Content: This is the summary of the third post and covers several issues. After this paragraph there's a More-Tag.

<!--more-->

This is a mardown file which is stored as `post3.md`.

This and the next paragraph are markdown text followed by to paragrpahs in a DIV

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque vero quas, eum architecto aliquid animi ipsum adipisci ut porro iure atque voluptatem sequi earum suscipit est aut ducimus, enim nihil asperiores dolores consectetur ex sed. Fugit quia excepturi nemo, cupiditate quam, sequi possimus sit. Quis atque modi illum cumque, ipsam.

<div>
    <p class="is-size-5">This and the next paragraph are in a DIV.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab dignissimos vel velit, quia perspiciatis quas. Voluptatibus cumque doloribus tenetur expedita veniam odio temporibus, nam minus perspiciatis maxime natus illo qui perferendis vitae. Vitae cumque nemo quasi vero, quod in non eum vel, fuga nulla quo officia ullam sunt sint tenetur exercitationem voluptas iusto perspiciatis? Pariatur praesentium inventore illum laboriosam beatae vitae dicta, quidem commodi recusandae nihil, unde enim a odio autem excepturi iste libero necessitatibus modi? Dolor officia optio obcaecati suscipit quas culpa saepe possimus nemo, beatae consequuntur quaerat nobis voluptatibus amet ex sunt debitis. Modi earum aut, illo voluptas.</p>
</div>

### This is a markdown heading (like H3)

Markdown and HTML can be mixed in a markdown document.
