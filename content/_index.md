+++
title       = "Homepage"
description = "The description of the homepage"
sonstiges   = "Here's some more text"

show_pages   = "" # Values 'all' or 'regular'

headline    = ""
+++

This is `content/_index.md` with a following Font Awesome Icon.

[//]: # (stil = s (solid) | r (regular) | l (light). Regular and Light only in Pro-Version)
[//]: # (icon = Iconname without fa-)
[//]: # (color = Color)
[//]: # (size = Values from 1 - 10)

Here's the Font Awesome Icon through a shortcode: {{< fonts/fontawesome style="s" icon="arrow-from-left" size="2" color="green" >}}
