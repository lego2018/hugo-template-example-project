+++
title       = "Imprint"
date        = 2017-07-14T15:40:34+01:00
description = "This is the imprint and very important"

bodyclass   = "imprint"

url          = "/imprint"

[menu.footer]
    weight = 10
+++

{{< elements/divider >}}

[//]: # (Here's a markdown comment which is not rendered in HTML.)

This is content from the shortcode `pages/human_sitemap.html`

{{< pages/human_sitemap >}}
