+++
title  = "Our Taxonomies"

layout = "taxonomy"

url    = "/taxonomies"
+++

This is the Taxonomies list page. It uses `layouts/pages/taxonomies.html` which is set manually in Front Matter.
