+++
title       = "Sitemap for humans"
date        = 2017-07-14T15:40:34+01:00
description = "This description comes from sitemap.md"

bodyclass   = "sitemap-human"

url          = "/sitemap"
+++

The sitemap is called with the shortcode `human_sitemap`.

{{< pages/human_sitemap >}}
