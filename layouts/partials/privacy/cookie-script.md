## Alternative: [Cookie Hinweis Script](https://website-tutor.com/cookie-plugin-script/)

## Alternative: [Cookie Consent](https://cookieconsent.insites.com/download/#)

## Alternative: [COOKIE INFO SCRIPT](https://cookieinfoscript.com/)

**Wichtiger Hinweis: Das Cookie-Banner darf wichtige Elemente wie z. B. die Links zum Impressum oder Datenschutz nicht verdecken! Das ist gesetzlich so vorgeschrieben.**

## Script für eine Darstellung am Seitenanfang kopieren
```html
<!-- Start Cookie Plugin -->
<script type="text/javascript">
  window.cookieconsent_options = {
  message: 'Diese Website nutzt Cookies, um bestmögliche Funktionalität zu bieten.',
  dismiss: 'Ok, verstanden',
  learnMore: 'Mehr Infos',
  link: 'http://domain.tld/datenschutz',
  theme: 'dark-top'
 };
</script>
<script type="text/javascript" src="//s3.amazonaws.com/valao-cloud/cookie-hinweis/script-v2.js"></script>
<!-- Ende Cookie Plugin -->
```

## Script für eine Darstellung unten rechts kopieren
```html
<!-- Start Cookie Plugin -->
<script type="text/javascript">
  window.cookieconsent_options = {
  message: 'Diese Website nutzt Cookies, um bestmögliche Funktionalität zu bieten.',
  dismiss: 'Ok, verstanden',
  learnMore: 'Mehr Infos',
  link: 'http://domain.tld/datenschutz',
  theme: 'dark-top'
 };
</script>
<script type="text/javascript" src="//s3.amazonaws.com/valao-cloud/cookie-hinweis/script-v2.js"></script>
<!-- Ende Cookie Plugin -->
```

## Script für eine Darstellung am Seitenende kopieren
```html
<!-- Start Cookie Plugin -->
<script type="text/javascript">
  window.cookieconsent_options = {
  message: 'Diese Website nutzt Cookies, um bestmögliche Funktionalität zu bieten.',
  dismiss: 'Ok, verstanden',
  learnMore: 'Mehr Infos',
  link: 'http://domain.tld/datenschutz',
  theme: 'dark-bottom'
 };
</script>
<script type="text/javascript" src="//s3.amazonaws.com/valao-cloud/cookie-hinweis/script-v2.js"></script>
<!-- Ende Cookie Plugin -->
```

## Script auf der eigenen Website platzieren
- Script vor dem schließenden </head> einfügen
- unbedingt in Zeile 7 die Url zu den eigenen Datenschutzbestimmungen einsetzen
- Informiere deine Nutzer über die Datenschutz-Grundverordnung für Webseiten in der EU. Nimm bestenfalls den entsprechenden [Cookie-Hinweis-Text mit DSGVO-Verweis](https://website-tutor.com/datenschutz/) in deine Datenschutzbestimmungen auf.
- Da das Script unter GNU GENERAL PUBLIC LICENSE v3 steht, darf es verändert und weitergegeben werden. Auch das hosten auf dem eigenen Server ist erlaubt. Dabei bitte beachten, dass es bei eigenen Änderungen unter gleicher Lizenz weitergegeben werden muss.

## Optionale Einstellungen zum Cookie Script
- Die Felder `message`, `dismiss` und `learnMore` können bei Bedarf angepasst werden
- Es stehen drei Erscheinungstypen zur Verfügung, die im Feld `theme` bestimmt werden: `dark-top`, `light-floating` und `dark-bottom`

## Button-Farbe ändern
Füge optional diese CSS-Klassen in deine `style.css` ein um Button-Farbe und andere Elemente des Cookie-Hinweises an die Gestaltung deiner Website anzupassen. Die Hinweise zwischen den `/* */` kannst du entfernen.

```css
.cc_container .cc_btn {
background-color: #e4e4e4 !important; /* Farbe des Buttons */
color: #000 !important; /* Textfarbe des Buttons */
}

.cc_container {
background: #1f6888 !important; /* Hintergrundfarbe des gesamten Bereichs */
color: #fdfdfd !important; /* Schriftfarbe des gesamten Bereichs */
}

.cc_container a {
color: #31a8f0 !important; /* Textlink-Farbe "Mehr Infos" */
}
```

Die Cookie-Laufzeit ist auf 365 Tage gesetzt. Mit einem Klick auf „Ok, verstanden“ bestätigt der Besucher die Kenntnisnahme zur Verwendung von Cookies auf deiner Website. Der Hinweis wird bei erneutem Besuch nicht wieder angezeigt, wenn der Cookie bis dahin nicht vom User gelöscht wurde, oder er die Seite mit einem anderen Browser aufruft.

Das Laden dieses Scripts allein löst nicht die Anforderungen der DSGVO 2018 in Bezug auf Speicherung und Verarbeitung personenbezogener Daten. Jeder Webmaster ist dazu aufgerufen die eigene Website nach Maßgabe der ab Mai 2018 wirksamen Datenschutz-Grundverordnung zu überprüfen. Dazu gehört beispielsweise die Überprüfung eingesetzter [Newsletter-Software](https://website-tutor.com/newsletter-software/) und CRM-Systemen.
