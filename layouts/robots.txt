User-agent: *
Disallow:

{{ range where .Data.Pages "Params.robotsdisallow" true }}
Disallow: {{ .RelPermalink }}
{{ end }}

Sitemap: {{ "sitemap.xml" | absLangURL }}
