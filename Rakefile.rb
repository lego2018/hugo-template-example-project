########################################################################################
# notify
#
#   Usage
#     rake notify
########################################################################################
task :notify => ["notify:pingomatic", "notify:google", "notify:bing"]
desc "Notify various services that the site has been updated"
namespace :notify do

  desc "Notify Ping-O-Matic"
  task :pingomatic do
    begin
      require 'xmlrpc/client'
      puts "* Notifying Ping-O-Matic that the site has updated"
      XMLRPC::Client.new('rpc.pingomatic.com', '/').call('weblogUpdates.extendedPing', 'www.domain.de' , '//www.domain.de', '//www.domain.de/atom.xml')
    rescue LoadError
      puts "! Could not ping ping-o-matic, because XMLRPC::Client could not be found."
    end
  end

  desc "Notify Google of updated sitemap"
  task :google do
    begin
      require 'net/http'
      require 'uri'
      puts "* Notifying Google that the site has updated"
      Net::HTTP.get('www.google.com', '/webmasters/tools/ping?sitemap=' + URI.escape('//www.domain.de/sitemap.xml'))
    rescue LoadError
      puts "! Could not ping Google about our sitemap, because Net::HTTP or URI could not be found."
    end
  end

  desc "Notify Bing of updated sitemap"
  task :bing do
    begin
      require 'net/http'
      require 'uri'
      puts '* Notifying Bing that the site has updated'
      Net::HTTP.get('www.bing.com', '/webmaster/ping.aspx?siteMap=' + URI.escape('//www.domain.de/sitemap.xml'))
    rescue LoadError
      puts "! Could not ping Bing about our sitemap, because Net::HTTP or URI could not be found."
    end
  end
end

########################################################################################
# rsync
#
#   Before usage:
#     Replace '$USER', '$HOST' and '$DIR' with your credentials
#
#   Usage
#     rake rsync:dryrun # If you're not sure, do dry-run
#     rake rsync:live # Rsync to your server
########################################################################################
namespace :rsync do
  desc "--dry-run rsync"
    task :dryrun do
      system('rsync build/production/ -avhe ssh --dry-run --delete --exclude \'.DS_Store\' $USER@$HOST:$DIR')
    end
  desc "rsync"
    task :live do
      system('rsync build/production/ -avhe ssh --delete --exclude \'.DS_Store\' $USER@$HOST:$DIR')
    end
end
