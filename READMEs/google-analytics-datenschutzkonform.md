# [Google Analytics Code datenschutzkonform in Ihre Website einbinden](https://www.website-optimieren.com/so-bauen-sie-google-analytics-richtig-in-ihre-unternehmenswebsite-ein/)

Schritt 1: Ihr altes Google Analytics Konto löschen

Schritt 2: Vertrag zur Auftragsdatenverarbeitung abschließen

Schritt 3: IP-Adressen anonymisieren im Google Analytics Code

Schritt 4: Code für Opt-Out-Cookie einbauen

Schritt 5: Opt-Out-Link einbauen

Schritt 6: Link zum Deaktivierungs-Add-on einbauen

Schritt 7: In der Datenschutzerklärung auf die Nutzung von Google Analytics hinweisen

