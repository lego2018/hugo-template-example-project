# [Auto Update The Copyright Year In Your Website](https://www.cssscript.com/auto-update-copyright-year/)

The simplest way to automatically update the copyright year in your website’s footer section using the [Date.prototype.getFullYear()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/getFullYear) API.

