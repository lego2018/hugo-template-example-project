# JavaScript effizient einbinden

## JavaScript im Partial oder Layout einbinden

JavaScript kann auch direkt in den Partials oder Layouts (z. B. `single.html`) eingebunden werden!

**Beispiel Partial**: Nur wenn z. B. das Partial `audio.html` eingebunden wird, sollen auch die Scripts erscheinen:

```
<div class = 'waves'>
  <canvas id = 'waves' class = 'waves_inner'></canvas>
</div>
<script src =  '{{ site.BaseURL }}js/sine-waves.min.js'></script>
<script src = '{{ site.BaseURL }}js/audio.js'></script>
```

**Beispiel Layout**

```
{{ define "main" }}
    <div>
        ...
    </div>

    <script src = '{{ "js/autosize.min.js" | absURL }}'></script>
    <script src = '{{ "js/timeago.js" | absURL }}'></script>
{{ end }}
```

## JavaScript im Partial `footer.html`

Vorteil von Einbinden des JavaScript in das Partial `footer.html`:

Wenn eine Seite keinen Footer hat, wird auch das JavaScript nicht unnötig geladen. Daher also nicht direkt in der `baseof.html` einbinden, sondern im Partial.
