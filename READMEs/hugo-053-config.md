# [Hugo Christmas Edition](https://gohugo.io/news/0.53-relnotes/)

## [How to setup config folder](https://github.com/gohugoio/hugo/pull/5501#issue-236237630)

This commit adds support for a configuration directory (default `config`). The different pieces in this puzzle are:

A new `--environment` (or `-e`) flag. This can also be set with the `HUGO_ENVIRONMENT` OS environment variable. The value for environment defaults to `production` when running `hugo` and `development` when running `hugo server`. You can set it to any value you want (e.g. `hugo server -e "Sensible Environment"`), but as it is used to load configuration from the file system, the letter case may be important. You can get this value in your templates with `{{ hugo.Environment }}`.
A new `--configDir` flag (defaults to `config` below your project). This can also be set with `HUGO_CONFIGDIR` OS environment variable.
If the `configDir` exists, the configuration files will be read and merged on top of each other from left to right; the right-most value will win on duplicates.

Given the example tree below:

If environment is `production`, the left-most `config.toml` would be the one directly below the project (this can now be omitted if you want), and then `_default/config.toml` and finally `production/config.toml`. And since these will be merged, you can just provide the environment specific configuration setting in you production config, e.g. `enableGitInfo = true`. The order within the directories will be lexical (`config.toml` and then `params.toml`).

```
config
├── _default
│   ├── config.toml
│   ├── languages.toml
│   ├── menus
│   │   ├── menus.en.toml
│   │   └── menus.zh.toml
│   └── params.toml
├── development
│   └── params.toml
└── production
    ├── config.toml
    └── params.toml
```

Some configuration maps support the language code in the filename (e.g. `menus.en.toml`): `menus` (menu also works) and `params`.

Also note that the only folders with "a meaning" in the above listing is the top level directories below `config`. The menus sub folder is just added for better organization.

We use TOML in the example above, but Hugo also supports JSON and YAML as configuration formats. These can be mixed.

## Global site and hugo var
Two new global variables in `site` and `hugo`. `hugo` gives you version info etc. (`{{ hugo.Version }}`, `{{ hugo.Environment }}`), but the `site` is probably more useful, as it allows you to access the current site’s variables (e.g. `{{ site.RegularPages }}`) without any context (or “.”).
