# [Style a markdown table with Bootstrap classes in Hugo](https://zwbetz.com/style-a-markdown-table-with-bootstrap-classes-in-hugo/)

To use it, pass the markdown table between the shortcode, then pass the Bootstrap table classes as an argument.

## Usage

```
{{< bootstrap-table "table table-dark table-striped table-bordered" >}}
| Animal  | Sounds |
|---------|--------|
| Cat     | Meow   |
| Dog     | Woof   |
| Cricket | Chirp  |
{{< /bootstrap-table >}}
```

## Definition

```
{{ $htmlTable := .Inner | markdownify }}
{{ $class := .Get 0 }}
{{ $old := "<table>" }}
{{ $new := printf "<table class=\"%s\">" $class }}
{{ $htmlTable := replace $htmlTable $old $new }}
{{ $htmlTable | safeHTML }}
```

