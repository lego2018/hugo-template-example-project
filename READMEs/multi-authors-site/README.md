# [Hugo tips: How to create author pages](https://www.netlify.com/blog/2018/07/24/hugo-tips-how-to-create-author-pages/)

## [Github Repository](https://github.com/imorente/hugo-multiauthor-example)

- Create authors taxonomy
- Add author metadata
- Create author templates
- Add authors to posts
- Example site

