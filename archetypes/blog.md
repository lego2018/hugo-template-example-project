+++
# This only looks weird but is rendered correct
title        = "{{ replace .TranslationBaseName "-" " " | title }}"
date         = {{ .Date }}
draft        = true
description  = "Description of page"
toc          = false
bodyclass    = "a-class-for-the-site"

categories   = ["Technology", "Hugo"]
tags         = ["Hello", "World"]

author       = "Firstname Lastname"

[page_image]
    src = "imagename"
    ext = "jpg"
    alt = "Image description"
+++

Before you continue, please take a moment to configure your archetypes.

Archetypes are located in the `archetypes` directory in the source of your site. To learn more about archetypes, visit [Archetypes](https://gohugo.io/content/archetypes/) on the Hugo website.

<!--more-->

This information appears below the [Summary Split](https://gohugo.io/content/summaries/).
