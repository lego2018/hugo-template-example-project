/**
        In 'package.json' are the following scripts:

        (npm) start         : bash-scripts/start.sh
        (npm) stop          : nps end
        (npm run) dev       : nps build.development
        (npm run) dev-disk  : nps build.development.hugo_disk
        (npm run) stage     : nps build.stage
        (npm run) local     : nps build.local
        (npm run) prod      : nps build.production
        (npm run) prod-nomin: nps build.production-nomin
        (npm run) img-update: nps helper.images
        (npm run) metricsOn : nps helper.metricsOn
        (npm run) metricsOff: nps helper.metricsOff

 */

module.exports = {
  scripts: {

    //----------------------------------------------------------//
    //    Start = bash-scripts/start.sh
    //----------------------------------------------------------//
    // 'start.sh' runs 'npm install', 'nps nodecopy' and 'nps copy'.
    // The 'nps -s' switch stands for 'silent' in the following tasks


    //----------------------------------------------------------//
    //    Resets
    //----------------------------------------------------------//
    end: "nps nodereset clean -s helper.htaccess -s helper.metricsOff",


    //----------------------------------------------------------//
    //    Builds for Development, Stage, Local and Production
    //----------------------------------------------------------//
    build: {
      // -- Development ----------
      //
      // CSS und JS are compiled by Hugo, the Hugo build includes Drafts
      // Future and Expired. The server is started with Hugo.
      //
      // The website is compiled in RAM.
      // With 'npm run dev-disk' it can also be rendederd on disk.
      // In that case you'll find the files in 'public'
      //
      // 'nps copy' and 'nps nodecopy' are started already with 'npm start'.
      development: {
        default: "nps -s build.development.hugo",

        // As of Hugo v.050 symlinks are still supported but you have to disable Browser Errors
        // Environment 'development' is automatically set
        hugo: "hugo server -wDEF --navigateToChanged",
        // Hugo renders to 'public'
        hugo_disk: "hugo server -wDEF --renderToDisk --navigateToChanged",
      },

      // -- Stage ----------
      // CSS und JS are compiled by Hugo, the Hugo build includes Drafts, Future and Expired.
      //
      // 'nps copy' and 'nps nodecopy' are started already with 'npm start'.
      stage: {
        default: "bash-scripts/stage.sh",
        // Set environment 'stage'
        hugo: "hugo -DEF -e stage",
      },

      // -- Local ----------
      // CSS und JS are compiled by Hugo. The website is compiled in 'build/local'.
      // The 'robots.txt' or 'humns.txt' are not needed for this build.
      //
      // 'nps copy' and 'nps nodecopy' are started already with 'npm start'.
      local: {
        default: "nps -s clean.local build.local.hugo clean.htaccesslocal",
        // Set environment 'local'
        hugo: "hugo -DEF -e local",
      },

      // -- Production ----------
      // The script 'bash-scripts/production.sh' starts after answering the questions
      // 'clean.production', 'build.production.hugo', 'build.production.minifyHtml' and 'build.production.minifyXml'.
      //
      // The website is compiled in 'build/production'.
      //
      // 'nps copy' and 'nps nodecopy' are started already with 'npm start'.
      production: {
        default: "bash-scripts/production.sh",
        // Environment 'production' is automatically set
        hugo: "hugo",

        // Minify HTML and XML
        // https://www.npmjs.com/package/html-minifier
        minifyHtml: "html-minifier --input-dir build/production --output-dir build/production --file-ext html --collapse-whitespace",
        minifyXml: "html-minifier --input-dir build/production --output-dir build/production --file-ext xml --collapse-whitespace",
      },

      // -- Production no Minification ----------
      // The script 'bash-scripts/production-nomin.sh' starts after answering the questions
      // 'clean.production' and 'build.productionNomin.hugo'.
      //
      // The website is compiled in 'build/production'.
      //
      // 'nps copy' and 'nps nodecopy' are started already with 'npm start'.
      productionNomin: {
        default: "bash-scripts/production-nomin.sh",

        hugo: "hugo",
      }
    },


    //----------------------------------------------------------//
    //    Copy
    //----------------------------------------------------------//
    copy: {
      // The '-s' switch is set in 'bash-scripts/start.sh'
      default: "nps copy.fav copy.images copy.elements",

      // If you’re using ditto to copy data from directories with symbolic links,
      // using the - V(verbose all) flag is valuable because it will display
      // every file and symbolic link that has been copied.Note - V is different than - v,
      // which will only show files as output, and not symbolic links.

      // Copy Favicons and delete README.md if there is one
      fav: "ditto -V assets/favicons/* static; [ -f static/README.md ]; del static/README.md",

      // Copy 'images', 'elements' and 'webfonts' to static
      images: "ditto -V assets/images static",
      elements: "ditto -V assets/elements static",

      // .htaccess copy to 'static'
      // This task is only called by 'bash-scripts/production.sh'
      // and 'bash-scripts/stage.sh'
      htaccess: "ditto -V assets/htaccess-custom static/.htaccess",
    },


    //----------------------------------------------------------//
    //    Node Modules
    //----------------------------------------------------------//
    // -- nodecopy ----------

    // The silent switch for nodecopy is set in
    // 'bash-scripts/start.sh'.
    nodecopy: {
      // Copies the needed JS files to 'assets/dependencies'.
      default: "bash-scripts/nodecopy.sh",
    },

    // -- nodereset ----------
    nodereset: {
      default: "nps -s nodereset.dependencies",

      // Discards 'assets/dependencies'
      dependencies: "del assets/dependencies/",
    },


    //----------------------------------------------------------//
    //    Cleaning
    //----------------------------------------------------------//
    clean: {
      default: "nps -s clean.static clean.stage clean.local clean.production clean.disk_public",

      // Empty 'static/' but preserve 'dependencies', 'elements', 'images' and 'webfonts
      // because they are symlinked from 'assets'.
      static: "del static/*; [ -f static/.htaccess ]; del static/.htaccess",

      // Empty 'build/dev/'
      dev: "del build/dev/*",

      // Empty 'build/stage/'
      stage: "del build/stage/*; [ -f build/stage/.htaccess ]; del build/stage/.htaccess",

      // Empty 'build/local/'
      local: "del build/local/*",

      // Discard '.htaccess' in 'build/local'
      htaccesslocal: "[ -f build/local/.htaccess ]; del build/local/.htaccess",

      // Empty 'build/production/'
      production: "del build/production/*; [ -f build/production/.htaccess ]; del build/production/.htaccess",

      // Discard 'public/'
      // Folder was generated by Hugo's render to disk
      disk_public: "[ -d public ]; del public",

      // Discard 'static/.htaccess'
      // Called by 'bash-scripts/stage.sh' and
      // 'bash-scripts/production.sh'
      htaccess: "[ -f static/.htaccess ]; del static/.htaccess",
    },


    //----------------------------------------------------------//
    //    Helper
    //----------------------------------------------------------//
    helper: {
      // 'robots.txt' is in 'layouts' and is processed by Hugo automatically.

      // Testing the Hugo-Site for Performance.
      metricsOn: "bash-scripts/template-metrics.sh",
      metricsOff: "[ -d public ]; del public",

      // Open Webbrowser
      open: "open http://localhost:1313",

      // Reset assets/htaccess-custom
      htaccess: "ditto -V assets/htaccess-reset assets/htaccess-custom",

      // Update images and elements
      images: "bash-scripts/img-update.sh",
    },
  },
};
